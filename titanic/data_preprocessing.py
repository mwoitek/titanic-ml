# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.13.1
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Data Pre-processing
# This notebook was created so I can experiment with different kinds of data transformation.
# ## Imports
# Run the following cell to import the code we'll use in this notebook:

# %%
import warnings

import numpy as np
from general.load_data import read_csv
from preprocessing import one_hot_encode, power_transform, standardization

# %%
warnings.filterwarnings("ignore")

# %% [markdown]
# ## Loading the Data Set
# DataFrame corresponding to the **training set**:

# %%
df_train = read_csv("train.csv")
assert df_train is not None
df_train.head(15)

# %% [markdown]
# ## Categorical Features
# Just checking that my functions for one-hot encoding work as intended.

# %%
df_testing = df_train[["Sex"]]
df_testing = one_hot_encode.one_hot_encode_feature(df_testing, "Sex", drop=False)
df_testing.head(15)

# %%
features = ["Pclass", "Sex", "Embarked"]
df_testing = df_train[features].dropna()
df_testing = one_hot_encode.one_hot_encode_features(df_testing, features, drop=False)
df_testing.head(15)

# %% [markdown]
# Everything seems to be OK!
# ## Numerical Features
# ### Age
# #### Power Transform

# %%
pt_age = power_transform.fit_power_transformer(df_train, "Age")
transformed_age = power_transform.power_transform(df_train, "Age", pt_age)
power_transform.power_transformed_histplot_widget("Age", transformed_age)

# %% [markdown]
# #### Standard Scaler
# Just checking that my functions work as intended.

# %%
ss_age = standardization.get_fitted_scaler(df_train, "Age")
standardization.scaler_info("Age", ss_age)

# %%
# pylint: disable=no-member
print(f"Mean: {df_train.Age.mean()}")
print(f"Variance: {df_train.Age.var(ddof=0)}")

# %%
scaled_age = standardization.scaler_transform(df_train, "Age", ss_age)
print(f"Mean: {np.nanmean(scaled_age)}")
print(f"Variance: {np.nanvar(scaled_age)}")

# %% [markdown]
# The StandardScaler code appears to be working correctly!
# #### Robust Scaler
# Just checking that my functions work as intended.

# %%
rs_age = standardization.get_fitted_scaler(df_train, "Age", scaler_class="robust")
standardization.scaler_info("Age", rs_age)

# %%
print(f"Median: {df_train.Age.median()}")
age_iqr = df_train.Age.quantile(q=0.75) - df_train.Age.quantile(q=0.25)
print(f"Interquartile range: {age_iqr}")

# %% [markdown]
# Everything seems to be OK!
# ### SibSp

# %%
pt_sibsp = power_transform.fit_power_transformer(df_train, "SibSp", method="yeo-johnson")
transformed_sibsp = power_transform.power_transform(df_train, "SibSp", pt_sibsp)
power_transform.power_transformed_histplot_widget("SibSp", transformed_sibsp)

# %% [markdown]
# Clearly, the power transformation doesn't improve the shape of the
# distribution. Then I have to find another way of dealing with this variable.
# ### Parch

# %%
pt_parch = power_transform.fit_power_transformer(df_train, "Parch", method="yeo-johnson")
transformed_parch = power_transform.power_transform(df_train, "Parch", pt_parch)
power_transform.power_transformed_histplot_widget("Parch", transformed_parch)

# %% [markdown]
# Clearly, the power transformation doesn't improve the shape of the
# distribution. Then I have to find another way of dealing with this variable.
