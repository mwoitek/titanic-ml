# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.13.1
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Dealing with Missing Values
# This notebook was created so I can check that my code for dealing with
# missing values works correctly.
# ## Imports
# Run the following cell to import the code we'll use in this notebook:

# %%
import warnings

import numpy as np
from general.load_data import read_csv
from missing_values import age, embarked
from missing_values.number_percentage import number_missing_values_column

# %%
warnings.filterwarnings("ignore")

# %% [markdown]
# ## Loading the Data Set
# DataFrame corresponding to the **training set**:

# %%
df_train = read_csv("train.csv")
assert df_train is not None
df_train.head(15)

# %% [markdown]
# DataFrame corresponding to the **test set**:

# %%
df_test = read_csv("test.csv")
assert df_test is not None
df_test.head(15)

# %% [markdown]
# ## Age

# %%
number_missing_values_column(df_train, "Age", "TRAINING SET")

# %% [markdown]
# The Age column has some missing values. To deal with this problem, we use the
# following strategy. First, we consider only the passengers with a known age.
# Then, for every combination of socioeconomic status and gender, we compute
# the corresponding median age. These are the results we utilize to fill the
# missing age values. More precisely, if a passenger has an unknown age, we
# look at its socioeconomic status and gender, and assign to its age the
# appropriate median value.

# %%
# Selecting some entries with missing age values:
tmp_df = df_train[["Pclass", "Sex", "Age"]].copy()
tmp_df = tmp_df[tmp_df["Age"].isna()]
tmp_df = tmp_df.iloc[:10, :]
tmp_df

# %%
# Computing the median ages:
median_ages = age.compute_median_ages(df_train, df_test)
median_ages

# %%
# Checking that the missing ages are filled correctly:
tmp_df = age.fill_missing_ages(tmp_df, median_ages)
tmp_df

# %%
df_train = age.fill_missing_ages(df_train, median_ages)
number_missing_values_column(df_train, "Age", "TRAINING SET")

# %%
del median_ages

# %% [markdown]
# Everything seems to be OK!
# ## Embarked
# In the training set, there are a few missing values in the Embarked column.
# Since the number of missing values is very small, we're not going to use an
# elaborate scheme for filling these values. They'll simply be replaced with
# the mode corresponding to Embarked.

# %%
# Values in the Embarked column:
print(np.sort(df_train["Embarked"].dropna().unique()))

# %% [markdown]
# * C = Cherbourg,
# * Q = Queenstown,
# * S = Southampton.

# %%
number_missing_values_column(df_train, "Embarked", "TRAINING SET")

# %%
# Mode corresponding to Embarked:
embarked_mode = embarked.get_mode(df_train)
print(f"Mode corresponding to Embarked: {embarked_mode}")

# %%
# Filling the missing Embarked values in the training set:
df_train = embarked.fill_missing_embarked(df_train)

# %%
number_missing_values_column(df_train, "Embarked", "TRAINING SET")

# %%
del embarked_mode
