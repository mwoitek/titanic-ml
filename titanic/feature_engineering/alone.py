from general import survival_death_rates
from numpy import ushort


def create_feature(df, drop=True):
    """Create "Alone" feature."""
    df["Alone"] = (df["SibSp"] == 0) & (df["Parch"] == 0)
    df["Alone"] = df["Alone"].astype(ushort)
    if drop:
        df = df.drop(columns=["SibSp", "Parch"])
    return df


def compute_survival_death_rates(df):
    """Compute survival and death rates."""
    feature = "Alone"
    categories_dict = {
        "Accompanied": 0,
        "Alone": 1,
    }
    return survival_death_rates.compute_survival_death_rates(df, feature, categories_dict)


def print_survival_death_rates(rates):
    """Print survival and death rates."""
    feature = "Alone"
    survival_death_rates.print_survival_death_rates(rates, feature)


def plot_survival_death_rates(rates):
    """Plot survival and death rates."""
    feature = "Alone"
    survival_death_rates.plot_survival_death_rates(rates, feature)
