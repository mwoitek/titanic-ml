import pandas as pd
from general import survival_death_rates
from IPython.display import display


def extract_form_address(df):
    """Extract the form of address from a passenger's name."""
    tmp_df = df["Name"].str.extract(r"\w+, (?:the )?(?P<FormOfAddress>\w+)\. .+")
    tmp_df["FormOfAddress"] = (
        tmp_df["FormOfAddress"]
        .replace(to_replace="Mlle", value="Miss")
        .replace(to_replace="Ms", value="Miss")
        .replace(to_replace="Mme", value="Mrs")
    )
    return tmp_df


def count_form_address(df_train, df_test):
    """Print the number of occurrences of all observed forms of address. It
    uses data from both training and test sets."""
    form_address = pd.concat([extract_form_address(df_train), extract_form_address(df_test)])
    display(form_address["FormOfAddress"].value_counts())


def create_feature(df, drop=True):
    """Create column containing forms of address. The forms of address with a
    small number of occurrences are replaced by "Special Title"."""
    df = pd.concat([df, extract_form_address(df)], axis="columns")
    df["FormOfAddress"] = df["FormOfAddress"].apply(
        lambda x: x if x in ["Mr", "Miss", "Mrs", "Master"] else "Special Title"
    )
    if drop:
        df = df.drop(columns="Name")
    return df


def master_describe_age(df_train, df_test):
    """Generate descriptive statistics for the ages of the passengers whose
    form of address is "Master"."""
    tmp_df_1 = df_train[["Name", "Age"]].copy()
    tmp_df_1 = pd.concat(
        [
            tmp_df_1,
            extract_form_address(df_train),
        ],
        axis="columns",
    ).drop(columns="Name")
    tmp_df_1 = tmp_df_1[tmp_df_1["FormOfAddress"] == "Master"].drop(columns="FormOfAddress")

    tmp_df_2 = df_test[["Name", "Age"]].copy()
    tmp_df_2 = pd.concat(
        [
            tmp_df_2,
            extract_form_address(df_test),
        ],
        axis="columns",
    ).drop(columns="Name")
    tmp_df_2 = tmp_df_2[tmp_df_2["FormOfAddress"] == "Master"].drop(columns="FormOfAddress")

    ages = pd.concat([tmp_df_1, tmp_df_2])["Age"]
    display(ages.describe())


def compute_survival_death_rates(df):
    """Compute survival and death rates."""
    feature = "FormOfAddress"
    categories_dict = {
        "Mr": "Mr",
        "Miss": "Miss",
        "Mrs": "Mrs",
        "Master": "Master",
        "Special Title": "Special Title",
    }
    return survival_death_rates.compute_survival_death_rates(df, feature, categories_dict)


def print_survival_death_rates(rates):
    """Print survival and death rates."""
    feature = "FormOfAddress"
    survival_death_rates.print_survival_death_rates(rates, feature)


def plot_survival_death_rates(rates):
    """Plot survival and death rates."""
    feature = "FormOfAddress"
    survival_death_rates.plot_survival_death_rates(rates, feature)
