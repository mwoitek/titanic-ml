from feature_engineering.form_address import extract_form_address
from general import survival_death_rates
from numpy import ushort


def create_feature(df):
    """Create "FemaleOrBoy" feature."""
    tmp_df = df[["FormOfAddress"]] if "FormOfAddress" in df.columns else extract_form_address(df)
    df["FemaleOrBoy"] = (df["Sex"] == "female") | (tmp_df["FormOfAddress"] == "Master")
    df["FemaleOrBoy"] = df["FemaleOrBoy"].astype(ushort)
    return df


def compute_survival_death_rates(df):
    """Compute survival and death rates."""
    feature = "FemaleOrBoy"
    categories_dict = {
        "Adult Males": 0,
        "Females or Boys": 1,
    }
    return survival_death_rates.compute_survival_death_rates(df, feature, categories_dict)


def print_survival_death_rates(rates):
    """Print survival and death rates."""
    feature = "FemaleOrBoy"
    survival_death_rates.print_survival_death_rates(rates, feature)


def plot_survival_death_rates(rates):
    """Plot survival and death rates."""
    feature = "FemaleOrBoy"
    survival_death_rates.plot_survival_death_rates(rates, feature)
