import ipywidgets as widgets
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from IPython.display import clear_output, display
from sklearn.preprocessing import PowerTransformer


def fit_power_transformer(df, feature, method="box-cox"):
    """Get a fitted power transformer for the given feature."""
    feature_np = df[feature].to_numpy()
    if method == "box-cox" and np.any(feature_np <= 0):
        return None
    pt = PowerTransformer(method=method)
    pt.fit(feature_np.reshape(-1, 1))
    return pt


def power_transform(df, feature, transformer):
    """Apply power transform to the given feature."""
    feature_np = df[feature].to_numpy().reshape(-1, 1)
    transformed_feature = transformer.transform(feature_np).flatten()
    return transformed_feature


def power_transformed_histplot(feature_name, transformed_feature, bins="auto"):
    """Plot histogram of a power transformed feature."""
    _, ax = plt.subplots(figsize=(12, 8))
    sns.histplot(x=transformed_feature, bins=bins, ax=ax)
    ax.set_xlabel(f"{feature_name} (power transformed)")
    ax.set_ylabel("Count")
    ax.set_title(f"Histogram of {feature_name} (power transformed)")
    plt.show()


def power_transformed_histplot_widget(feature_name, transformed_feature):
    """Generate a widget for creating a histogram of a power transformed feature."""
    slider_bins = widgets.IntSlider(
        description="Bins:",
        value=7,
        min=2,
        max=40,
        step=1,
        disabled=False,
        continuous_update=True,
        orientation="horizontal",
        readout=True,
        readout_format="d",
    )
    display(slider_bins)

    out = widgets.Output()
    display(out)
    with out:
        power_transformed_histplot(feature_name, transformed_feature, bins=slider_bins.value)

    def on_value_change(change):
        with out:
            clear_output()
            power_transformed_histplot(feature_name, transformed_feature, bins=change["new"])

    slider_bins.observe(on_value_change, names="value")
