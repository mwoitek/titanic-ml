"""Standardization of Numerical Features"""
from sklearn.preprocessing import RobustScaler, StandardScaler


def get_fitted_scaler(df_train, feature, scaler_class="standard"):
    """Get fitted scaler by using data in the training set."""
    feature_np = df_train[feature].to_numpy().reshape(-1, 1)
    if scaler_class == "standard":
        scaler = StandardScaler()
    elif scaler_class == "robust":
        scaler = RobustScaler()
    scaler.fit(feature_np)
    return scaler


def scaler_transform(df, feature, fitted_scaler):
    """Use a fitted scaler to transform the given feature."""
    feature_np = df[feature].to_numpy().reshape(-1, 1)
    scaled_feature = fitted_scaler.transform(feature_np).flatten()
    return scaled_feature


def standard_scaler_info(fitted_scaler, feature_name=None):
    """Print relevant attributes associated with a StandardScaler."""
    if feature_name is not None:
        print(f"Feature: {feature_name}")
    print(f"Mean: {fitted_scaler.mean_[0]}")
    print(f"Variance: {fitted_scaler.var_[0]}")


def robust_scaler_info(fitted_scaler, feature_name=None):
    """Print relevant attributes associated with a RobustScaler."""
    if feature_name is not None:
        print(f"Feature: {feature_name}")
    print(f"Median: {fitted_scaler.center_[0]}")
    print(f"Interquartile range: {fitted_scaler.scale_[0]}")


def scaler_info(fitted_scaler, feature_name=None):
    """Print relevant attributes associated with a scaler."""
    if isinstance(fitted_scaler, StandardScaler):
        print("StandardScaler")
        standard_scaler_info(fitted_scaler, feature_name)
    elif isinstance(fitted_scaler, RobustScaler):
        print("RobustScaler")
        robust_scaler_info(fitted_scaler, feature_name)
