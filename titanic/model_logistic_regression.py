# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.13.1
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# # A Very Simple Classification Model
# In this notebook, I'm going to create and train a very simple model for
# predicting who survived the disaster. Here the main goal is to develop the
# basis for working with other more refined classification models.
# ## Imports
# Run the following cell to import the code we'll use in this notebook:

# %%
import warnings

from feature_engineering import alone, female_or_boy, form_address
from general.load_data import read_csv
from missing_values import age as missing_age
from missing_values import embarked as missing_embarked
from missing_values.number_percentage import no_missing_value
from modeling import data_splitting, logistic_regression, model_evaluation
from preprocessing import one_hot_encode, standardization

# %%
# from missing_values.number_percentage import number_missing_values

# %%
warnings.filterwarnings("ignore")

# %% [markdown]
# ## Loading the Data Set
# DataFrame corresponding to the **training set**:

# %%
df_train = read_csv("train.csv")
assert df_train is not None

# %%
# df_train.head(15)

# %%
# pylint: disable=no-member
# df_train.info()

# %% [markdown]
# DataFrame corresponding to the **test set**:

# %%
df_test = read_csv("test.csv")
assert df_test is not None

# %%
# df_test.head(15)

# %%
# df_test.info()

# %% [markdown]
# ## Remove Columns

# %%
# Columns that won't be used:
columns_to_drop = [
    "Ticket",
    "Fare",
    "Cabin",
]

# %%
# Remove columns from the training set:
df_train = df_train.drop(columns=columns_to_drop)

# %%
# df_train.info()

# %%
# Remove columns from the test set:
df_test = df_test.drop(columns=columns_to_drop)

# %%
# df_test.info()

# %% [markdown]
# ## Dealing with Missing Values
# ### Age
# The Age column has some missing values. To deal with this problem, we use the
# following strategy. First, we consider only the passengers with a known age.
# Then, for every combination of socioeconomic status and gender, we compute
# the corresponding median age. These are the results we utilize to fill the
# missing age values. Summarizing: if a passenger has an unknown age, we look
# at its socioeconomic status and gender, and assign to its age the appropriate
# median value.

# %%
# Computing the median ages:
median_ages = missing_age.compute_median_ages(df_train, df_test)

# %%
# median_ages

# %%
# Filling the missing Age values:
df_train = missing_age.fill_missing_ages(df_train, median_ages)
df_test = missing_age.fill_missing_ages(df_test, median_ages)
del median_ages

# %% [markdown]
# ### Embarked
# In the training set, there are a few missing values in the Embarked column.
# Since the number of missing values is very small, we're not going to use an
# elaborate scheme for filling these values. They'll simply be replaced with
# the mode corresponding to Embarked.

# %%
# Filling the missing Embarked values in the training set:
df_train = missing_embarked.fill_missing_embarked(df_train)

# %% [markdown]
# ### Check: No missing value left

# %%
# number_missing_values(df_train, "TRAINING SET")

# %%
# number_missing_values(df_test, "TEST SET")

# %%
assert no_missing_value(df_train)
assert no_missing_value(df_test)

# %% [markdown]
# ## Feature Engineering
# ### Alone
# This binary feature tells us if a passenger was traveling alone or
# accompanied.

# %%
df_train = alone.create_feature(df_train, drop=True)

# %%
# df_train.head(15)

# %%
df_test = alone.create_feature(df_test, drop=True)

# %%
# df_test.head(15)

# %% [markdown]
# ### FormOfAddress
# To create this feature, we begin by using the names of the passengers to
# extract their forms of address. The four titles with the most number of
# occurrences are kept. Then the remaining titles are replaced by "Special
# Title". By doing so, we create a categorical feature with five categories.

# %%
df_train = form_address.create_feature(df_train, drop=True)

# %%
# df_train.head(15)

# %%
df_test = form_address.create_feature(df_test, drop=True)

# %%
# df_test.head(15)

# %% [markdown]
# ### FemaleOrBoy
# This binary feature tells us if a given passenger was a female/young male or
# an adult male.

# %%
df_train = female_or_boy.create_feature(df_train)

# %%
# df_train.head(15)

# %%
df_test = female_or_boy.create_feature(df_test)

# %%
# df_test.head(15)

# %% [markdown]
# ## Data Pre-processing
# ### Categorical Features
# #### One-hot Encoding

# %%
# List of categorical features that need to be one-hot encoded:
categorical_features = [
    "Pclass",
    "Sex",
    "Embarked",
    "FormOfAddress",
]

# %%
df_train = one_hot_encode.one_hot_encode_features(df_train, categorical_features, drop=False)
df_test = one_hot_encode.one_hot_encode_features(df_test, categorical_features, drop=False)

# %% [markdown]
# ##### Pclass
# Just checking that this feature was encoded correctly:

# %%
# columns = ["Pclass", "Pclass_0", "Pclass_1", "Pclass_2"]
# df_train[columns].head(15)

# %%
# df_test[columns].head(15)

# %% [markdown]
# ##### Sex
# Just checking that this feature was encoded correctly:

# %%
# columns = ["Sex", "Sex_0", "Sex_1"]
# df_train[columns].head(15)

# %%
# df_test[columns].head(15)

# %% [markdown]
# ##### Embarked
# Just checking that this feature was encoded correctly:

# %%
# columns = ["Embarked", "Embarked_0", "Embarked_1", "Embarked_2"]
# df_train[columns].head(15)

# %%
# df_test[columns].head(15)

# %% [markdown]
# ##### FormOfAddress
# Just checking that this feature was encoded correctly:

# %%
# columns = [
#     "FormOfAddress",
#     "FormOfAddress_0",
#     "FormOfAddress_1",
#     "FormOfAddress_2",
#     "FormOfAddress_3",
#     "FormOfAddress_4",
# ]
# df_train[columns].head(15)

# %%
# df_test[columns].head(15)

# %%
# del columns

# %%
df_train = df_train.drop(columns=categorical_features)
df_test = df_test.drop(columns=categorical_features)
del categorical_features

# %% [markdown]
# ### Numerical Features
# #### Age
# Standardizing this feature:

# %%
scaler_class = "standard"
scaler_age = standardization.get_fitted_scaler(df_train, "Age", scaler_class)
del scaler_class

# %%
# standardization.scaler_info(scaler_age, "Age")

# %%
df_train["ScaledAge"] = standardization.scaler_transform(df_train, "Age", scaler_age)

# %%
# df_train[["Age", "ScaledAge"]].head(3)

# %%
df_test["ScaledAge"] = standardization.scaler_transform(df_test, "Age", scaler_age)
del scaler_age

# %%
# df_test[["Age", "ScaledAge"]].head(3)

# %%
df_train = df_train.drop(columns="Age")
df_test = df_test.drop(columns="Age")

# %% [markdown]
# ### Data for Modeling and Prediction
# Final form of the DataFrame corresponding to the **training set**:

# %%
# df_train.head(15)

# %%
# df_train.info()

# %% [markdown]
# Final form of the DataFrame corresponding to the **test set**:

# %%
# df_test.head(15)

# %%
# df_test.info()

# %% [markdown]
# ## Cross Validation Set
# Training data: Separate features from the target, and create cross validation
# set.

# %%
target = "Survived"
features = [
    "Alone",
    "FemaleOrBoy",
    "Pclass_0",
    "Pclass_1",
    "Pclass_2",
    "Sex_0",
    "Sex_1",
    "Embarked_0",
    "Embarked_1",
    "Embarked_2",
    "FormOfAddress_0",
    "FormOfAddress_1",
    "FormOfAddress_2",
    "FormOfAddress_3",
    "FormOfAddress_4",
    "ScaledAge",
]

# %%
# Split the training data:
ratio = 0.25
x_train, x_cv, y_train, y_cv = data_splitting.split_train_cv(
    df_train,
    target,
    features=features,
    ratio=ratio,
)

# %%
# Just checking that the splitting was done correctly:
# print(f"x_train shape: {x_train.shape}")
# print(f"x_cv shape:    {x_cv.shape}")
# print(f"y_train shape: {y_train.shape}")
# print(f"y_cv shape:    {y_cv.shape}")

# %% [markdown]
# ## Logistic Regression Classifier

# %%
# Grid Search
param_grid = {
    "penalty": ["l1", "l2"],
    "C": [0.01, 0.1, 1, 10, 100],
    "class_weight": [None, "balanced"],
    "solver": ["liblinear"],
    "max_iter": [500],
}
scoring = "f1"

# %%
lr_clf = logistic_regression.get_fitted_classifier_grid_search(
    x_train,
    y_train,
    param_grid,
    scoring,
)
del param_grid
del scoring

# %%
print(lr_clf.best_params_)

# %%
# Parameters of the classifier constructor:
clf_params = {
    "random_state": 0,
    "solver": "liblinear",
    "max_iter": 500,
}

# %%
# Use the training data to get the fitted classifier:
lr_clf = logistic_regression.get_fitted_classifier(x_train, y_train, **clf_params)
del clf_params

# %%
# Print the coefficients of the features:
logistic_regression.print_coefficients(lr_clf)

# %% [markdown]
# ### Model Evaluation

# %%
# Use the features in the cross validation set to get the probabilities and
# predictions:
threshold = 0.5
df_pred = logistic_regression.predict_with_threshold(lr_clf, x_cv, threshold)

# %%
# df_pred.head(15)

# %%
# Compute and print the elements of the confusion matrix:
model_evaluation.elements_confusion_matrix(y_cv, df_pred["Prediction"])

# %%
# Plot the confusion matrix:
model_evaluation.plot_confusion_matrix(
    y_cv,
    df_pred["Prediction"],
    class_names=["Died", "Survived"],
)

# %%
# Compute and print the accuracy:
model_evaluation.accuracy(y_cv, df_pred["Prediction"])

# %%
# Compute and print the precision:
model_evaluation.precision(y_cv, df_pred["Prediction"])

# %%
# Compute and print the recall:
model_evaluation.recall(y_cv, df_pred["Prediction"])

# %%
# Print a report containing several important classification metrics:
model_evaluation.print_classification_report(
    y_cv,
    df_pred["Prediction"],
    class_names=["Died", "Survived"],
)

# %% [markdown]
# #### Precision-Recall Curve

# %%
model_evaluation.precision_recall_plot(lr_clf, x_cv, y_cv)

# %%
model_evaluation.precision_recall_auc(y_cv, df_pred["Probability"])

# %% [markdown]
# #### ROC Curve

# %%
model_evaluation.roc_plot(lr_clf, x_cv, y_cv)

# %%
model_evaluation.roc_auc(y_cv, df_pred["Probability"])

# %% [markdown]
# ### Predictions for the Test Set

# %%
threshold = 0.5
logistic_regression.predict_save_csv(lr_clf, df_test, threshold)
