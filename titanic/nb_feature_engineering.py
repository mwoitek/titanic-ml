# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.13.1
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Feature Engineering
# This notebook was created so I can experiment with the creation of new
# features.
# ## Imports
# Run the following cell to import the code we'll use in this notebook:

# %%
import warnings

from feature_engineering import alone, female_or_boy, form_address
from general.load_data import read_csv

# %%
warnings.filterwarnings("ignore")

# %% [markdown]
# ## Loading the Data Set
# DataFrame corresponding to the **training set**:

# %%
df_train = read_csv("train.csv")
assert df_train is not None
df_train.head(15)

# %% [markdown]
# DataFrame corresponding to the **test set**:

# %%
df_test = read_csv("test.csv")
assert df_test is not None
df_test.head(15)

# %% [markdown]
# ## FormOfAddress

# %%
# Print the number of occurrences of all observed forms of address:
form_address.count_form_address(df_train, df_test)

# %%
# Generate descriptive statistics for the ages of the passengers whose form of
# address is "Master":
form_address.master_describe_age(df_train, df_test)

# %% [markdown]
# Clearly, the passengers considered above are boys. Also notice that there are 8
# missing age values. Perhaps we can use a special strategy for filling these
# values.

# %%
# Select only the features that are relevant in this case:
tmp_df = df_train[["Name", "Survived"]].copy()

# %%
# Create categorical feature from the forms of address:
tmp_df = form_address.create_feature(tmp_df, drop=False)
tmp_df.head(15)

# %%
# Compute and print survival and death rates:
rates = form_address.compute_survival_death_rates(tmp_df)
form_address.print_survival_death_rates(rates)

# %%
# Plot survival and death rates:
form_address.plot_survival_death_rates(rates)

# %% [markdown]
# ## FemaleOrBoy

# %%
# Select only the features that are relevant in this case:
tmp_df = df_train[["Name", "Sex", "Survived"]].copy()

# %%
# Create column containing forms of address:
tmp_df = form_address.create_feature(tmp_df, drop=False)
tmp_df.head(15)

# %%
# Add FemaleOrBoy feature:
tmp_df = female_or_boy.create_feature(tmp_df)
tmp_df.head(15)

# %%
# Compute and print survival and death rates:
rates = female_or_boy.compute_survival_death_rates(tmp_df)
female_or_boy.print_survival_death_rates(rates)

# %%
# Plot survival and death rates:
female_or_boy.plot_survival_death_rates(rates)

# %% [markdown]
# ## Alone

# %%
# Select only the features that are relevant in this case:
tmp_df = df_train[["SibSp", "Parch", "Survived"]].copy()

# %%
# Add Alone feature:
tmp_df = alone.create_feature(tmp_df)
tmp_df.head(15)

# %%
# Compute and print survival and death rates:
rates = alone.compute_survival_death_rates(tmp_df)
alone.print_survival_death_rates(rates)

# %%
# Plot survival and death rates:
alone.plot_survival_death_rates(rates)
