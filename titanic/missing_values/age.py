import pandas as pd

# The Age column has some missing values. To deal with this problem, we use the
# following strategy. First, we consider only the passengers with a known age.
# Then, for every combination of socioeconomic status and gender, we compute
# the corresponding median age. These are the results we utilize to fill the
# missing age values. More precisely, if a passenger has an unknown age, we
# look at its socioeconomic status and gender, and assign to its age the
# appropriate median value.


def compute_median_ages(df_train, df_test):
    """Compute the median ages. Uses data from both training and test sets."""
    tmp_df_1 = df_train[["Pclass", "Sex", "Age"]].copy()
    tmp_df_1 = tmp_df_1[tmp_df_1["Age"].notna()]

    tmp_df_2 = df_test[["Pclass", "Sex", "Age"]].copy()
    tmp_df_2 = tmp_df_2[tmp_df_2["Age"].notna()]

    median_ages = pd.concat([tmp_df_1, tmp_df_2]).groupby(by=["Pclass", "Sex"]).median()
    return median_ages


def fill_missing_ages(df, median_ages):
    """Fill missing age values."""
    tmp_df = df[df["Age"].isna()]
    tmp_df_idx = tmp_df.index.to_numpy()
    ages = [
        median_ages.at[(tmp_df.at[i, "Pclass"], tmp_df.at[i, "Sex"]), "Age"] for i in tmp_df_idx
    ]
    df.loc[tmp_df_idx, "Age"] = ages
    return df
