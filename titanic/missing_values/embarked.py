# In the training set, there are a few missing values in the Embarked column.
# Since the number of missing values is very small, we're not going to use an
# elaborate scheme for filling these values. They'll simply be replaced with
# the mode corresponding to Embarked.


def get_mode(df):
    """Get the mode corresponding to Embarked."""
    embarked_mode = df["Embarked"].mode().iat[0]
    return embarked_mode


def fill_missing_embarked(df):
    """Fill the missing Embarked values."""
    df["Embarked"] = df["Embarked"].fillna(value=get_mode(df))
    return df
