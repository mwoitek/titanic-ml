from IPython.display import display


def number_missing_values_column(df, column, label=None):
    """Print the number of missing values in a specific column of the DataFrame."""
    if label is not None:
        print(label)
    print(f"{column} - Number of missing values: {df[column].isna().sum()}")


def number_missing_values(df, label=None):
    """Print the number of missing values for every column of a DataFrame."""
    if label is not None:
        print(label)
    print("Number of missing values for every column:")
    display(df.isna().sum())


def percentage_missing_values(df, label=None):
    """Print the percentage of missing values for every column of a DataFrame."""
    if label is not None:
        print(label)
    print("Percentage of missing values for every column:")
    display((100 * df.isna().sum() / len(df)).apply(lambda x: f"{x:.2f}%"))


def no_missing_value(df):
    """Returns True if the DataFrame has no missing value. Otherwise, returns False."""
    answer = True
    for column in df.columns:
        if df[column].isna().sum() > 0:
            answer = False
            break
    return answer
