from pathlib import Path

import pandas as pd


def get_data_folder():
    """Get the path to the folder containing the data files."""
    data_folder = Path(".").resolve() / "data"
    if data_folder.exists() and data_folder.is_dir():
        return data_folder
    return None


def read_csv(file_name):
    """Create a Pandas DataFrame by reading a CSV file."""
    data_folder = get_data_folder()
    if data_folder is None:
        return None

    file_path = data_folder / file_name
    if not file_path.exists() or not file_path.is_file():
        return None

    return pd.read_csv(file_path, index_col="PassengerId")
