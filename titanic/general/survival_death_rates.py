import matplotlib.pyplot as plt
import numpy as np


def compute_survival_death_rates(df, feature, categories_dict):
    """Compute survival and death rates."""
    if not feature in df.columns:
        return None

    counts = df[["Survived", feature]].groupby(feature)["Survived"].value_counts()

    def compute_rates_1_category(category):
        counts_category = (
            counts.loc[(category, slice(None))].droplevel(feature).sort_index(ascending=False)
        )
        total = counts_category.sum()
        return (100 * counts_category / total).to_numpy()

    survival_death_rates = {
        label: compute_rates_1_category(value) for label, value in categories_dict.items()
    }
    return survival_death_rates


def print_survival_death_rates(survival_death_rates, feature):
    """Print survival and death rates."""
    print("SURVIVAL AND DEATH RATES")
    print(f"Feature: {feature}")
    for label, rates in survival_death_rates.items():
        print(f"--- {label} ---")
        print(f"Survival Rate: {rates[0]:.2f}%")
        print(f"Death Rate:    {rates[1]:.2f}%")


def plot_survival_death_rates(survival_death_rates, feature):
    """Plot survival and death rates."""
    _, ax = plt.subplots(figsize=(12, 8))
    labels = list(survival_death_rates.keys())
    x = np.arange(len(labels))
    width = 0.3
    survival_rates = [survival_death_rates[label][0] for label in survival_death_rates.keys()]
    death_rates = [survival_death_rates[label][1] for label in survival_death_rates.keys()]
    ax.bar(x - width / 2, survival_rates, width, label="Survival Rate")
    ax.bar(x + width / 2, death_rates, width, label="Death Rate")
    ax.set_xticks(x)
    ax.set_xticklabels(labels)
    ax.set_xlabel("")
    ax.set_ylabel("Rate")
    ax.set_title(f"{feature} - Survival and Death Rates")
    ax.legend()
    plt.show()
