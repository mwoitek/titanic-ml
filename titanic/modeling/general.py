from pathlib import Path
from time import time_ns

import pandas as pd
from numpy import ushort
from sklearn.model_selection import GridSearchCV


def get_fitted_classifier(x_train, y_train, clf_class, **clf_params):
    """Use the training data to get a fitted classifier."""
    clf = clf_class(**clf_params)
    clf.fit(x_train, y_train)
    return clf


def get_fitted_classifier_grid_search(x_train, y_train, clf_class, param_grid, scoring):
    estimator = clf_class(random_state=0)
    clf = GridSearchCV(estimator, param_grid, scoring=scoring)
    clf.fit(x_train, y_train)
    return clf


def predict_with_threshold(clf, x_test, threshold=0.5):
    """Use the fitted classifier to get a DataFrame containing the
    probabilities and predictions."""
    probs = clf.predict_proba(x_test)[:, 1]
    y_pred = (probs >= threshold).astype(ushort)
    return pd.DataFrame(
        data={
            "Probability": probs,
            "Prediction": y_pred,
        },
        index=x_test.index,
    )


def predict_save_csv(file_name_prefix, clf, x_test, threshold=0.5):
    """Use the fitted classifier to make predictions for the test data, and
    then save these predictions to a CSV file."""
    df_pred = predict_with_threshold(clf, x_test, threshold)
    df_pred = (
        df_pred.drop(columns="Probability")
        .rename(columns={"Prediction": "Survived"})
        .reset_index()
    )
    folder_path = Path(".").resolve() / "predictions"
    file_path = folder_path / f"{file_name_prefix}_{time_ns()}.csv"
    df_pred.to_csv(path_or_buf=file_path, index=False)
