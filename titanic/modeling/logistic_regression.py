import pandas as pd
from IPython.display import display
from modeling import general
from sklearn.linear_model import LogisticRegression


def get_fitted_classifier(x_train, y_train, **clf_params):
    """Use the training data to get a fitted classifier."""
    clf_class = LogisticRegression
    return general.get_fitted_classifier(x_train, y_train, clf_class, **clf_params)


def get_fitted_classifier_grid_search(x_train, y_train, param_grid, scoring):
    clf_class = LogisticRegression
    return general.get_fitted_classifier_grid_search(
        x_train, y_train, clf_class, param_grid, scoring
    )


def print_coefficients(clf, label=None):
    """Print the coefficients of the features in descending order."""
    coeffs = clf.coef_[0, :]
    idx_sort = coeffs.argsort()[::-1]
    if label is not None:
        print(label)
    df_coeffs = pd.DataFrame(
        data={
            "Feature": clf.feature_names_in_[idx_sort],
            "Coefficient": coeffs[idx_sort],
        },
    )
    display(df_coeffs)


def predict_with_threshold(clf, x_test, threshold=0.5):
    """Use the fitted classifier to get a DataFrame containing the
    probabilities and predictions."""
    return general.predict_with_threshold(clf, x_test, threshold)


def predict_save_csv(clf, x_test, threshold=0.5):
    """Use the fitted classifier to make predictions for the test data, and
    then save these predictions to a CSV file."""
    file_name_prefix = "logistic_regression"
    general.predict_save_csv(file_name_prefix, clf, x_test, threshold)
