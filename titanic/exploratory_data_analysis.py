# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.13.1
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Titanic: Machine Learning from Disaster - Exploratory Data Analysis
# ## Imports
# Run the following cell to import the code we'll use in this notebook:

# %%
import warnings

from eda import age, pclass, survived
from general.load_data import read_csv
from missing_values.number_percentage import number_missing_values, percentage_missing_values

# %%
warnings.filterwarnings("ignore")

# %% [markdown]
# ## Loading the Data Set
# DataFrame corresponding to the **training set**:

# %%
df_train = read_csv("train.csv")
assert df_train is not None
df_train.head(15)

# %%
df_train.info()  # pylint: disable=no-member

# %% [markdown]
# DataFrame corresponding to the **test set**:

# %%
df_test = read_csv("test.csv")
assert df_test is not None
df_test.head(15)

# %%
df_test.info()  # pylint: disable=no-member

# %% [markdown]
# ## Missing Values
# Missing values in the **training set**:

# %%
number_missing_values(df_train, "TRAINING SET")

# %%
percentage_missing_values(df_train, "TRAINING SET")

# %% [markdown]
# Missing values in the **test set**:

# %%
number_missing_values(df_test, "TEST SET")

# %%
percentage_missing_values(df_test, "TEST SET")

# %% [markdown]
# ## Number of Passengers

# %%
def number_passengers(df, label):
    """Print the number of passengers."""
    print(f"Total number of passengers in the {label}: {len(df)}")


number_passengers(df_train, "TRAINING SET")
number_passengers(df_test, "TEST SET")

# %% [markdown]
# ## Number of Survivors and Casualties (Training Set)

# %%
# Print the number of survivors and casualties:
survived.survived_died(df_train)

# %%
# Print the percentage of survivors and casualties:
survived.survived_died(df_train, percentage=True)

# %%
# Widget for plotting the number/percentage of survivors and casualties:
survived.create_plot_widget(df_train)

# %% [markdown]
# ## Pclass
# "Pclass" is a proxy for socioeconomic status. The possible values for this
# categorical variable are
# * 1 = Upper class,
# * 2 = Middle class,
# * 3 = Lower class.
#
# Passengers in the **training set**:

# %%
# Print the number of passengers for every class:
pclass.passengers_pclass(df_train, "TRAINING SET")

# %%
# Plot the number of passengers for every class:
pclass.passengers_pclass_barplot(df_train, "Training Set")

# %%
# Print the percentage of passengers for every class:
pclass.passengers_pclass(df_train, "TRAINING SET", percentage=True)

# %%
# Plot the percentage of passengers for every class:
pclass.passengers_pclass_barplot(df_train, "Training Set", percentage=True)

# %% [markdown]
# Passengers in the **test set**:

# %%
# Print the number of passengers for every class:
pclass.passengers_pclass(df_test, "TEST SET")

# %%
# Plot the number of passengers for every class:
pclass.passengers_pclass_barplot(df_test, "Test Set")

# %%
# Print the percentage of passengers for every class:
pclass.passengers_pclass(df_test, "TEST SET", percentage=True)

# %%
# Plot the percentage of passengers for every class:
pclass.passengers_pclass_barplot(df_test, "Test Set", percentage=True)

# %% [markdown]
# ### Socioeconomic Status and Survival (Training Set)

# %%
# Print the number of survivors and casualties for every class:
pclass.number_passengers_pclass_survived_died(df_train)

# %%
# Plot the number of survivors and casualties for every class:
pclass.number_passengers_pclass_survived_died_countplot(df_train)

# %%
# Compute and print survival and death rates for every class:
pclass_rates = pclass.compute_pclass_rates(df_train)
pclass.print_pclass_rates(pclass_rates)

# %%
# Plot survival and death rates for every socioeconomic class:
pclass.plot_pclass_rates(pclass_rates)

# %%
del pclass_rates

# %% [markdown]
# ## Age

# %%
# For now, we are going to ignore the missing values:
age_no_missing_train = age.remove_missing_values(df_train)
age_no_missing_test = age.remove_missing_values(df_test)

# %% [markdown]
# Passengers in the **training set**:

# %%
# Widget for creating a boxplot of the age:
age.age_boxplot_widget(age_no_missing_train, "Training Set")

# %%
# Widget for creating a histogram of the age:
age.age_histplot_widget(age_no_missing_train, "Training Set")

# %% [markdown]
# Passengers in the **test set**:

# %%
# Widget for creating a boxplot of the age:
age.age_boxplot_widget(age_no_missing_test, "Test Set")

# %%
# Widget for creating a histogram of the age:
age.age_histplot_widget(age_no_missing_test, "Test Set")

# %%
# del age_no_missing_train
# del age_no_missing_test
