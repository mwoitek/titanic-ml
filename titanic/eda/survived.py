import ipywidgets as widgets
import matplotlib.pyplot as plt
import seaborn as sns
from IPython.display import clear_output, display


def survived_died(df, percentage=False):
    """Print the number/percentage of survivors and casualties."""
    values = df["Survived"].value_counts(normalize=percentage)
    if percentage:
        values = (100 * values).apply(lambda x: f"{x:.2f}%")
    dict_values = {
        "casualties": values.at[0],
        "survivors": values.at[1],
    }
    value_type = "Percentage" if percentage else "Number"
    for casualties_survivors, value in dict_values.items():
        print(f"{value_type} of {casualties_survivors}: {value}")


def survived_died_barplot(df, percentage=False):
    """Plot the number/percentage of survivors and casualties."""
    _, ax = plt.subplots(figsize=(12, 8))
    values = df["Survived"].value_counts(normalize=percentage)
    if percentage:
        values *= 100
    sns.barplot(
        x=["Casualties", "Survivors"],
        y=[values.at[0], values.at[1]],
        ax=ax,
    )
    ax.set_xlabel("")
    value_type = "Percentage" if percentage else "Number"
    ax.set_ylabel(f"{value_type} of Passengers")
    ax.set_title(f"{value_type} of Survivors and Casualties")
    plt.show()


def create_plot_widget(df):
    """Create a widget for plotting the number/percentage of survivors and casualties."""
    buttons = widgets.RadioButtons(
        description="Select what to plot:",
        options=["Count", "Percentage"],
        value="Count",
        disabled=False,
    )
    display(buttons)

    out = widgets.Output()
    display(out)
    with out:
        survived_died_barplot(df)

    def on_value_change(change):
        percentage = change["new"] == "Percentage"
        with out:
            clear_output()
            survived_died_barplot(df, percentage)

    buttons.observe(on_value_change, names="value")
