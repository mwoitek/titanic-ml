import ipywidgets as widgets
import matplotlib.pyplot as plt
import seaborn as sns
from IPython.display import clear_output, display


def age_describe(age, label=None):
    """Generate descriptive statistics for the values in the Age column."""
    if label is not None:
        print(label)
    display(age.describe())


def remove_missing_values(df):
    """Remove the missing values in the Age column."""
    return df.loc[:, "Age"].dropna()


def age_boxplot(age, label, show_outliers=True):
    """Create the boxplot of the age."""
    _, ax = plt.subplots(figsize=(12, 8))
    sns.boxplot(y=age.to_numpy(), showfliers=show_outliers, ax=ax)
    ax.set_xticks([])
    ax.set_ylabel("Age")
    ax.set_title(f"{label} - Boxplot of the Age")
    plt.show()


def age_histplot(age, label, bins="auto"):
    """Create the histogram of the age."""
    _, ax = plt.subplots(figsize=(12, 8))
    sns.histplot(x=age.to_numpy(), bins=bins, ax=ax)
    ax.set_xlabel("Age")
    ax.set_ylabel("Count")
    ax.set_title(f"{label} - Histogram of the Age")
    plt.show()


def age_boxplot_widget(age, label):
    """Generate a widget for creating a boxplot of the age."""
    cb_show_outliers = widgets.Checkbox(
        description="Show outliers",
        value=True,
        disabled=False,
        indent=False,
    )
    display(cb_show_outliers)

    cb_print_summary = widgets.Checkbox(
        description="Print summary",
        value=True,
        disabled=False,
        indent=False,
    )
    display(cb_print_summary)

    out_plot = widgets.Output()
    display(out_plot)
    with out_plot:
        age_boxplot(age, label)

    out_summary = widgets.Output()
    display(out_summary)
    with out_summary:
        age_describe(age)

    def on_change_cb_show_outliers(change):
        with out_plot:
            clear_output()
            age_boxplot(age, label, show_outliers=change["new"])

    cb_show_outliers.observe(on_change_cb_show_outliers, names="value")

    def on_change_cb_print_summary(change):
        out_summary.layout.display = "block" if change["new"] else "none"

    cb_print_summary.observe(on_change_cb_print_summary, names="value")


def age_histplot_widget(age, label):
    """Generate a widget for creating a histogram of the age."""
    slider_bins = widgets.IntSlider(
        description="Bins:",
        value=7,
        min=2,
        max=40,
        step=1,
        disabled=False,
        continuous_update=True,
        orientation="horizontal",
        readout=True,
        readout_format="d",
    )
    display(slider_bins)

    out = widgets.Output()
    display(out)
    with out:
        age_histplot(age, label, bins=slider_bins.value)

    def on_value_change(change):
        with out:
            clear_output()
            age_histplot(age, label, bins=change["new"])

    slider_bins.observe(on_value_change, names="value")
