import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns


def passengers_pclass(df, label=None, percentage=False):
    """Print the number/percentage of passengers for every socioeconomic class."""
    values = df["Pclass"].value_counts(normalize=percentage)
    if percentage:
        values = (100 * values).apply(lambda x: f"{x:.2f}%")
    dict_values = {
        "UPPER": values.at[1],
        "MIDDLE": values.at[2],
        "LOWER": values.at[3],
    }
    if label is not None:
        print(label)
    value_type = "Percentage" if percentage else "Number"
    for class_name, value in dict_values.items():
        print(f"{value_type} of passengers in the {class_name} class: {value}")


def passengers_pclass_barplot(df, label, percentage=False):
    """Plot the number/percentage of passengers for every socioeconomic class."""
    _, ax = plt.subplots(figsize=(12, 8))
    values = df["Pclass"].value_counts(normalize=percentage)
    if percentage:
        values *= 100
    sns.barplot(
        x=["Upper", "Middle", "Lower"],
        y=[values.at[1], values.at[2], values.at[3]],
        ax=ax,
    )
    ax.set_xlabel("Socioeconomic Class")
    value_type = "Percentage" if percentage else "Number"
    ax.set_ylabel(f"{value_type} of Passengers")
    ax.set_title(f"{label} - {value_type} of Passengers for Every Socioeconomic Class")
    plt.show()


def number_passengers_pclass_survived_died(df):
    """Print the number of survivors and casualties for every socioeconomic class."""
    counts = df.loc[:, ["Survived", "Pclass"]].groupby(by="Survived")["Pclass"].value_counts()
    counts_died = counts.loc[(0, slice(None))].droplevel("Survived")
    counts_survived = counts.loc[(1, slice(None))].droplevel("Survived")

    def create_print_dict(counts_survived_died, label):
        dict_survived_died = {
            "UPPER": counts_survived_died.at[1],
            "MIDDLE": counts_survived_died.at[2],
            "LOWER": counts_survived_died.at[3],
        }
        print(f"{label}:")
        for class_name, num_passengers in dict_survived_died.items():
            print(f"{class_name} class: {num_passengers}")

    create_print_dict(counts_died, "Casualties")
    create_print_dict(counts_survived, "Survivors")


def number_passengers_pclass_survived_died_countplot(df):
    """Plot the number of survivors and casualties for every socioeconomic class."""
    _, ax = plt.subplots(figsize=(12, 8))
    sns.countplot(
        x="Pclass",
        data=df,
        order=np.sort(df["Pclass"].unique()),
        hue="Survived",
        hue_order=np.sort(df["Survived"].unique()),
        ax=ax,
    )
    ax.set_xticklabels(["Upper", "Middle", "Lower"])
    ax.set_xlabel("Socioeconomic Class")
    ax.set_ylabel("Number of Passengers")
    ax.set_title("Number of Survivors and Casualties for Every Socioeconomic Class")
    ax.legend(["Casualties", "Survivors"])
    plt.show()


def compute_survival_death_rates(counts, class_number):
    """Compute survival and death rates."""
    survived_counts = (
        counts.loc[(class_number, slice(None))].droplevel("Pclass").sort_index(ascending=False)
    )
    total = survived_counts.sum()
    survival_death_rates = (100 * survived_counts / total).to_numpy()
    return survival_death_rates


def compute_pclass_rates(df):
    """Compute survival and death rates for every socioeconomic class."""
    counts = df.loc[:, ["Survived", "Pclass"]].groupby(by="Pclass")["Survived"].value_counts()
    pclass_rates = {
        "UPPER CLASS": compute_survival_death_rates(counts, class_number=1),
        "MIDDLE CLASS": compute_survival_death_rates(counts, class_number=2),
        "LOWER CLASS": compute_survival_death_rates(counts, class_number=3),
    }
    return pclass_rates


def print_pclass_rates(pclass_rates):
    for label, rates in pclass_rates.items():
        print(label)
        print(f"Survival Rate: {rates[0]:.2f}%")
        print(f"Death Rate:    {rates[1]:.2f}%")


def plot_pclass_rates(pclass_rates):
    """Plot survival and death rates for every socioeconomic class."""
    _, ax = plt.subplots(figsize=(12, 8))
    lbls = ["Upper", "Middle", "Lower"]
    x = np.arange(len(lbls))
    width = 0.3
    class_survival_rates = [
        pclass_rates["UPPER CLASS"][0],
        pclass_rates["MIDDLE CLASS"][0],
        pclass_rates["LOWER CLASS"][0],
    ]
    class_death_rates = [
        pclass_rates["UPPER CLASS"][1],
        pclass_rates["MIDDLE CLASS"][1],
        pclass_rates["LOWER CLASS"][1],
    ]
    ax.bar(x - width / 2, class_survival_rates, width, label="Survival Rate")
    ax.bar(x + width / 2, class_death_rates, width, label="Death Rate")
    ax.set_xticks(x)
    ax.set_xticklabels(lbls)
    ax.set_xlabel("Socioeconomic Class")
    ax.set_ylabel("Rate")
    ax.set_title("Survival and Death Rates for Every Socioeconomic Class")
    ax.legend()
    plt.show()
